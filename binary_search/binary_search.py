# Binary search works by continuously dividing a set of items until the searched for value is found
#                                0  1  2  3  4   5   6   7   8   9   Indices
# For example...in the array of [1, 2, 5, 6, 9, 13, 21, 33, 45, 66] find 33
# 1. The list of items(numbers here) must be sorted
# 2. Variables to use: left, middle, right indices and a middle value
# 3. Then add left + right indices and divide by 2, take the floor value...this will become the middle value
# 4. Use middle value to check if it's equal to the search value
# 5. If middle value equals search value return middle index
# 6. If middle value is less than search value then make left value equal middle + 1
# 7. Recalculate the middle value...(left + right) / 2
# 8. Repeat steps 4-8 until value is found


int_list = [1, 2, 5, 6, 9, 13, 21, 33, 45, 66]


def binary_search(search_value):
    print(int_list)
    left_idx = 0
    right_idx = len(int_list)-1
    middle_idx = (left_idx + right_idx) // 2
    middle_value = int_list[middle_idx]

    while left_idx <= right_idx:
        if middle_value == search_value:
            return middle_idx
        elif middle_value < search_value:
            left_idx = middle_idx + 1
            middle_idx = (left_idx + right_idx) // 2
            middle_value = int_list[middle_idx]
        else:
            right_idx = middle_idx - 1
            middle_idx = (left_idx + right_idx) // 2
            middle_value = int_list[middle_idx]


def recalculate_middle_idx(left, right):
    return left


print("Found value at index: ", binary_search(90))
