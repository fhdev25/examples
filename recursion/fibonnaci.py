# The fibonacci sequence n is found by adding the (n-1) + (n-2) numbers
def fibonacci_seq(num):
    if num <= 1:
        return num
    else:
        return fibonacci_seq(num - 1) + fibonacci_seq(num - 2)


# Prompt user for number of n sequences to print
n_series = 0
entered_value = input("Enter number of fibonnaci sequence to print: ")

if entered_value is None or entered_value == '':
    print("Empty value entered...defaulting to 5")
    n_series = 5
else:
    n_series = int(entered_value)

if n_series <= 0:
    print("Value entered must be greater than 0...defaulting to 5")
    n_series = 5

print("The fibonacci series for {} is: ".format(n_series))
for n in range(n_series):
    print("{} ".format(fibonacci_seq(n)), end="")
